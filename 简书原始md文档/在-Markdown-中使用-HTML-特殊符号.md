> Source: [CSD - 在markdown中使用 HTML 中的特殊符号](http://blog.csdn.net/vola9527/article/details/69948411)

符号|代码|符号|代码|符号|代码  
:----:|:----|:---:|:--- |:---:|:---- 
&Alpha;	|`&Alpha;`|	&Beta;	|`&Beta;`|	&Gamma;	|`&Gamma;`
&Delta;	|`&Delta;`|	&Epsilon;	|`&Epsilon;`|	&Zeta;	|`&Zeta;`
&Eta;	|`&Eta;`|	&Theta;	|`&Theta;`|	&Iota;	|`&Iota;`
&Kappa;	|`&Kappa;`|	&Lambda;	|`&Lambda;`|	&Mu;	|`&Mu;`
&Nu;	|`&Nu;`|	&Xi;	|`&Xi;`|	&Omicron;	|`&Omicron;`
&Pi;	|`&Pi;`|	&Rho;	|`&Rho;`|	&Sigma;	|`&Sigma;`
&Tau;	|`&Tau;`|	&Upsilon;	|`&Upsilon;`|	&Phi;	|`&Phi;`
&Chi;	|`&Chi;`|	&Psi;	|`&Psi;`|	&Omega;	|`&Omega;`
&alpha;	|`&alpha;`|	&beta;	|`&beta;`|	&gamma;	|`&gamma;`
&delta;	|`&delta;`|	&epsilon;	|`&epsilon;`|	&zeta;	|`&zeta;`
&eta;	|`&eta;`|	&theta;	|`&theta;`|	&iota;	|`&iota;`
&kappa;	|`&kappa;`|	&lambda;	|`&lambda;`|	&mu;	|`&mu;`
&nu;	|`&nu;`|	&xi;	|`&xi;`|	&omicron;	|`&omicron;`
&pi;	|`&pi;`|	&rho;	|`&rho;`|	&sigmaf;	|`&sigmaf;`
&sigma;	|`&sigma;`|	&tau;	|`&tau;`|	&upsilon;	|`&upsilon;`
&phi;	|`&phi;`|	&chi;	|`&chi;`|	&psi;	|`&psi;`
&omega;	|`&omega;`|	&thetasym;	|`&thetasym;`|	&upsih;	|`&upsih;`
&piv;	|`&piv;`|	&bull;	|`&bull;`|	&hellip;	|`&hellip;`
&prime;	|`&prime;`|	&Prime;	|`&Prime;`|	&oline;	|`&oline;`
&frasl;	|`&frasl;`|	&weierp;	|`&weierp;`|	&image;	|`&image;`
&real;	|`&real;`|	&trade;	|`&trade;`|	&alefsym;	|`&alefsym;`
&larr;	|`&larr;`|	&uarr;	|`&uarr;`|	&rarr;	|`&rarr;`
&darr;	|`&darr;`|	&harr;	|`&harr;`|	&crarr;	|`&crarr;`
&lArr;	|`&lArr;`|	&uArr;	|`&uArr;`|	&rArr;	|`&rArr;`
&dArr;	|`&dArr;`|	&hArr;	|`&hArr;`|	&forall;	|`&forall;`
&part;	|`&part;`|	&exist;	|`&exist;`|	&empty;	|`&empty;`
&nabla;	|`&nabla;`|	&isin;	|`&isin;`|	&notin;	|`&notin;`
&ni;	|`&ni;`|	&prod;	|`&prod;`|	&sum;	|`&sum;`
&minus;	|`&minus;`|	&lowast;	|`&lowast;`|	&radic;	|`&radic;`
&prop;	|`&prop;`|	&infin;	|`&infin;`|	&ang;	|`&ang;`
&and;	|`&and;`|	&or;	|`&or;`|	&cap;	|`&cap;`
&cup;	|`&cup;`|	&int;	|`&int;`|	&there4;	|`&there4;`
&sim;	|`&sim;`|	&cong;	|`&cong;`|	&asymp;	|`&asymp;`
&ne;	|`&ne;`|	&equiv;	|`&equiv;`|	&le;	|`&le;`
&ge;	|`&ge;`|	&sub;	|`&sub;`|	&sup;	|`&sup;`
&nsub;	|`&nsub;`|	&sube;	|`&sube;`|	&supe;	|`&supe;`
&oplus;	|`&oplus;`|	&otimes;	|`&otimes;`|	&perp;	|`&perp;`
&sdot;	|`&sdot;`|	&lceil;	|`&lceil;`|	&rceil;	|`&rceil;`
&lfloor;	|`&lfloor;`|	&rfloor;	|`&rfloor;`|	&loz;	|`&loz;`
&spades;	|`&spades;`|	&clubs;	|`&clubs;`|	&hearts;	|`&hearts;`
&diams;	|`&diams;`|	&nbsp;	|`&nbsp;`|	&iexcl;	|`&iexcl;`
&cent;	|`&cent;`|	&pound;	|`&pound;`|	&curren;	|`&curren;`
&yen;	|`&yen;`|	&brvbar;	|`&brvbar;`|	&sect;	|`&sect;`
&uml;	|`&uml;`|	&copy;	|`&copy;`|	&ordf;	|`&ordf;`
&laquo;	|`&laquo;`|	&not;	|`&not;`|	&shy;	|`&shy;`
&reg;	|`&reg;`|	&macr;	|`&macr;`|	&deg;	|`&deg;`
&plusmn;	|`&plusmn;`|	&sup2;	|`&sup2;`|	&sup3;	|`&sup3;`
&acute;	|`&acute;`|	&micro;	|`&micro;`|	&quot;	|`&quot;`
&lt;	|`&lt;`|	&gt;	|`&gt;`|		|


---
---
---
> Stata 寒假班  **报名中……**
连玉君主讲，`2018年1月13日-21日`，北京）   
[Stata**初级班**](http://www.peixun.net/view/307.html) ｜ [Stata**高级班**](http://www.peixun.net/view/308.html)  |  [Stata**全程班**](http://www.peixun.net/view/1135.html) 
---
---
---

### 往期回顾

- [Stata帮助和网络资源汇总(持续更新中)](http://www.jianshu.com/p/c723bb0dbf98)
- [协整：醉汉牵着一条狗](http://www.jianshu.com/p/08e194e14b7a)
- [在 Markdown 中快速插入文字连接](http://www.jianshu.com/p/ff3b1fa07a97)
- [Stata dofile 转换 PDF 制作讲义方法](http://www.jianshu.com/p/b119033d8b93)
- [Github使用方法及Stata资源](http://www.jianshu.com/p/d2ee76b0f74c)
- [码云：我把常用小软件都放这儿了](http://www.jianshu.com/p/fbb6bdeb9df5)
- [连玉君的链接](http://www.jianshu.com/p/494e6feab565)
- [Stata小抄：一组图记住Stata常用命令](http://www.jianshu.com/p/6b1e387f172b)



![Stata连享会二维码](http://wx1.sinaimg.cn/mw690/8abf9554gy1fj9p14l9lkj20m30d50u3.jpg "扫码关注 Stata 连享会")
