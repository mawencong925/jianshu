> 李缘 | |  Stata 连享会 ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [github](http://github.com/StataChina))

### 方法一：Writage  + Pandoc -- 双剑合璧！

1. 下载并安装 `Writage`，下载地址：http://www.writage.com/

- 打开 [Writage网页](http://www.writage.com/)，点击`Download`，再点击`Download Now`完成下载

> ![网页](http://upload-images.jianshu.io/upload_images/7692714-0dea7d7aeeffba56.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "1.png")

> ![下载](http://upload-images.jianshu.io/upload_images/7692714-6ce19b9714bb0336.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "2.png")

- 运行安装程序，一般按照默认选项安装就好啦

>![安装](http://upload-images.jianshu.io/upload_images/7692714-b10e96c9c32e0026.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "3.png")

- 重启电脑，新建或打开任一 `Word` 文档，在 `文件` 菜单栏下选 `另存为`，查看 **【保存类型】** 中是否有 `Markdown` 格式。
  （如果插件安装成功，就会自动出现`Markdown`选项；否则，重新安装一遍吧~）

>![输入图片说明](http://upload-images.jianshu.io/upload_images/7692714-180fec4da16b8f9f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "4.png")


2. 下载并安装 `Pandoc`：[官方下载地址](http://pandoc.org/installing.html  ) ；  [百度云下载地址](https://pan.baidu.com/s/1nvMA1Rz)

- 运行安装程序，一般按照默认选项安装就好啦

> ![下载](http://upload-images.jianshu.io/upload_images/7692714-200a8ac8072f02c2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "5.png")


3. 准备好工具啦，我们开始尝试将word文档转换为markdown文档吧！

-  **首先设置 word 文档中的标准样式**，如一级、二级标题，项目符号或编号等，如此才能与 markdown 的格式对应
  （稍微有点繁琐的前期准备，如果文档一开始就是按照标准样式排版，就没有这个烦恼啦）

> ![设置](http://upload-images.jianshu.io/upload_images/7692714-abb1b0c891b37773.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "6.png")


- `Word` 格式 **另存为** `Markdown`（这是最关键的一步~）

> ![另存为](http://upload-images.jianshu.io/upload_images/7692714-783e08901ab25077.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "7.png")

- `markdown` 文档在 `Word` 中的显示效果：

![image](http://upload-images.jianshu.io/upload_images/7692714-e545bfbe49f09069.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "8.png")

  由于markdown中的图片无法设置大小，因此在word中排布的图片格式不标准，需要人工调整

  其他格式，如一级、二级标题，项目列表等基本没有问题，其中表格显示如下

![image](http://upload-images.jianshu.io/upload_images/7692714-80ae8d0c8d8f306d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "9.png")

- 将`markdown`文档上传至码云等平台并提交，对应的内容预览效果如下

![image](http://upload-images.jianshu.io/upload_images/7692714-570250456c4049f4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "10.png")

![image](http://upload-images.jianshu.io/upload_images/7692714-99c848242acc487a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "11.png")


4.大功告成啦！ :laughing: 


---
#### 更加快捷（有时略坑）的方法二：`Word to Markdown Converter`在线转换网页！

1. **设置word文档中的标准样式，如一级、二级标题，项目符号或编号等，如此才能与markdown的格式对应**

  （稍微有点繁琐的前期准备，如果文档一开始就是按照标准样式排版，就没有这个烦恼啦）

![设置](http://upload-images.jianshu.io/upload_images/7692714-9dfa2efd15ca0a63.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "6.png")

2. 打开网页`Word to Markdown Converter`：https://word-to-markdown.herokuapp.com/

![输入图片说明](http://upload-images.jianshu.io/upload_images/7692714-a8bc48ed32e7907e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "12.png")

3. 选择需要转换的`Word`文件,点击`Convert`

![输入图片说明](http://upload-images.jianshu.io/upload_images/7692714-a2998ff04f5745ab.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "13.png")

4.大功告成啦！ :yum: 页面左边显示的是`Markdown`文档的内容，右边显示的是预览出来的样子

![输入图片说明](http://upload-images.jianshu.io/upload_images/7692714-692e722f34b3be99.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "14.png")

 **那么坑在哪里呢？** 我们会发现，表格转换出来是这样子的 :disappointed: 

`Markdown`文档

![输入图片说明](http://upload-images.jianshu.io/upload_images/7692714-235cbd951c925c95.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "15.png")

预览出来

![输入图片说明](http://upload-images.jianshu.io/upload_images/7692714-ea29513995c11fea.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "16.png")

- **因此还需要进一步手动编辑整理完善表格** 

（如果，`Word`文档本身不包括表格，`Word to Markdown Converter`使用体验可谓相当便捷稳妥啦！）

---
#### 图片的下载与存储

除了标准格式设置与表格调整问题， **图片的下载与存储**也会是我们可能会遇到的问题


-  **Markdown转换为Word** ：在`Markdown`文档中，图片以网络超链接的形式保存,如果markdown文档中有这一类图片，那么需要在网络连接的情况下，才能正常输出有图片的word文档。否则，图片处显示空白


- **Word转换为Markdown** ：`Word`转换为`Markdown`文档之后，文档中的图片输出到本地文件夹下，将该文件夹与输出的`Markdown`文档在同一目录下，在`Markdown`文档中图片引用本地相对路径，也就是说，必须保证`Markdown`文档与存放图片的本地文件夹在一起，才能完整的在markdown编辑器中显示图片
 
---

参考资料：

[软件技能|markdown与word相互转换的快捷方法](http://www.jianshu.com/p/f9c5da56e0cb)

[怎么把word转成markdown](http://www.jianshu.com/p/b0be43b03015)


---
---
---
> Stata 寒假班  **报名中……**
连玉君主讲，`2018年1月13日-21日`，北京）   
[Stata**初级班**](http://www.peixun.net/view/307.html)&ensp;｜&ensp;[Stata**高级班**](http://www.peixun.net/view/308.html)&ensp; | &ensp;[Stata**全程班**](http://www.peixun.net/view/1135.html) 
---
---
---


>#### 关于我们
- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [【简书-Stata连享会】](http://www.jianshu.com/u/69a30474ef33) 和 [【知乎-连玉君Stata专栏】](https://www.zhihu.com/people/arlionn)。可以在**简书**和**知乎**中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 推文中的相关数据和程序，以及 [Markdown 格式原文](https://gitee.com/arlionn/jianshu) 可以在 [【Stata连享会-码云】](https://gitee.com/arlionn) 中获取。[【Stata连享会-码云】](https://gitee.com/arlionn) 中还放置了诸多 Stata 资源和程序。如 [Stata命令导航](https://gitee.com/arlionn/stata/wikis/Home) ||  [stata-fundamentals](https://gitee.com/arlionn/stata-fundamentals) ||  [Propensity-score-matching-in-stata](https://gitee.com/arlionn/propensity-score-matching-in-stata) || [Stata-Training](https://gitee.com/arlionn/StataTraining) 等。


>#### 联系我们
- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com


---
![欢迎加入Stata连享会(公众号: StataChina)](http://upload-images.jianshu.io/upload_images/7692714-275edc2bfff1a392.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")
