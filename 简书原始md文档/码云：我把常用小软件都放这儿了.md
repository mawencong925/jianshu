> 作者：连玉君 ( [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [github](http://github.com/StataChina) )



----
----
-  [我的常用软件*下载地址*](https://gitee.com/arlionn/mysoftware) 
----
----

---
---
---
> Stata 寒假班  **报名中……**
连玉君主讲，`2018年1月13日-21日`，北京）   
[Stata**初级班**](http://www.peixun.net/view/307.html) ｜ [Stata**高级班**](http://www.peixun.net/view/308.html)  |  [Stata**全程班**](http://www.peixun.net/view/1135.html) 
---
---
---
> 刚建立了一个 [Stata连享会-Stata Wikis](https://gitee.com/arlionn/stata/wikis/Home)
> 以便快速查询常用 Stata 命令和 Blog 资源

用了一段时间 `github`，速度还行，但我担心日后访问有问题。

没想到国内也有类似的产品——`码云`。

很方便，日后出门不用带 U 盘了，因为我可以随时访问我的 `码云` 账户，被我设置为 `公开项目` 的内容，无需登陆即可访问。这比百度云盘之类的方便多了。

比如，我常用的一些小软件都放在了 [这里](https://gitee.com/arlionn/mysoftware) 。大家也可以前往下载。

![Stata连享会-常用小软件-码云](http://upload-images.jianshu.io/upload_images/7692714-8dfa0b0c43147dd1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

同时，我在**简书**上发布的文章都是用 Markdown 写的，这些原始的 `Markdown` 文件也都统一放在了 `码云` 中，方便对写 Markdown 有兴趣的朋友查阅。[简书-Stata连享会 Markdown 文档](https://gitee.com/arlionn/jianshu)。

![简书-Stata连享会 Markdown 文档](http://upload-images.jianshu.io/upload_images/7692714-95cd33f31bf637cb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

我还从 `github` 上搬来了一些不错的 Stata 项目，比如：
- [Stata基础教程及范例](https://gitee.com/arlionn/stata-fundamentals)
- [Stata小抄PDF版-习题](https://gitee.com/arlionn/StataTraining)

更重要的是，如果我们合作写论文的话，可以建立一个 `私有项目`，然后大家把数据、dofile、word文档之类的拖拽到码云上即可，就不用再频繁地 `发邮件` → `另存为` → `发邮件` 了。

---
---
---
> Stata 寒假班  **报名中……**
连玉君主讲，`2018年1月13日-21日`，北京）   
[Stata**初级班**](http://www.peixun.net/view/307.html) ｜ [Stata**高级班**](http://www.peixun.net/view/308.html)  |  [Stata**全程班**](http://www.peixun.net/view/1135.html) 
---
---

---
![Stata连享会二维码](http://upload-images.jianshu.io/upload_images/7692714-acf0f11a78cdf788.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")


>## 关于我们
- 【**Stata 连享会**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [【简书-Stata连享会】](http://www.jianshu.com/u/69a30474ef33) 和 [【知乎-连玉君Stata专栏】](https://www.zhihu.com/people/arlionn)。可以在**简书**和**知乎**中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 推文中的相关数据和程序，以及 [Markdown 格式原文](https://gitee.com/arlionn/jianshu) 可以在 [【Stata连享会-码云】](https://gitee.com/arlionn) 中获取。[【Stata连享会-码云】](https://gitee.com/arlionn) 中还放置了诸多 Stata 资源和程序。如 [Stata命令导航](https://gitee.com/arlionn/stata/wikis/Home) ||  [stata-fundamentals](https://gitee.com/arlionn/stata-fundamentals) ||  [Propensity-score-matching-in-stata](https://gitee.com/arlionn/propensity-score-matching-in-stata) || [Stata-Training](https://gitee.com/arlionn/StataTraining) 等。


>### 联系我们
- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com
