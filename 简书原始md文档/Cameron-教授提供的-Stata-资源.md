


A. Colin Cameron 教授 [主页](http://cameron.econ.ucdavis.edu/) 上提供的 Stata 资源：

*   A very brief introduction to Getting started in Windows Stata is:    [stataintro.html](http://cameron.econ.ucdavis.edu/stata/stataintro.html)

*   The Stata website has video tutorials: see [https://www.stata.com/links/video-tutorials/](https://www.stata.com/links/video-tutorials/)  
   **In particular see:**  
    - Stata Basics: Tour of the Stata Interface
    - Descriptive statistics, tables, and cross-tabulations: Descriptive Statistics
    - Linear Models: Simple linear regression
    - Classical hypothesis tests: t test for two independent samples

*   A good web source for how to use Stata is:    [http://www.ats.ucla.edu/stata/](https://stats.idre.ucla.edu/stata/) 

###### Many of the following resources are oriented towards graduate-level research.
*   The American Economic Association has developed a site on how to use Stata (as well as LIMDEP and SAS).
    See [https://www.aeaweb.org/about-aea/committees/economic-education/econometrics-training-modules](https://www.aeaweb.org/about-aea/committees/economic-education/econometrics-training-modules)

*   The text A.Colin Cameron and Pravin K. Trivedi (2009, revised edition 2010), [Microeconometrics using Stata](http://cameron.econ.ucdavis.edu/musbook/mus.html), Stata Press,     focuses on use of Stata for modern regression analysis of cross-section and panel data.     
**All the original edition programs (in Stata 10.1) and datasets are available for easy download.**   
**All the revised edition programs (in Stata 11) and datasets are available for easy download.**
    See [http://cameron.econ.ucdavis.edu/musbook/mus.html](http://cameron.econ.ucdavis.edu/musbook/mus.html)

*   The more advanced text A. Colin Cameron and Pravin K. Trivedi (2005), "Microeconomettrics: Methods and Applications", Cambridge University Press, focuses more on methods with applications.
    This uses mostly Stata 8 and all the book programs and datasets are available for download.

    See: [http://cameron.econ.ucdavis.edu/mmabook/mmaprograms.html](http://cameron.econ.ucdavis.edu/mmabook/mmaprograms.html)

*   In October 1999 I wrote up on-line help files on getting started in Stata.
    This uses mostly Stata 7.
    See: [http://cameron.econ.ucdavis.edu/stata/stata1999.html](http://cameron.econ.ucdavis.edu/stata/stata1999.html)

*   Several other **books on regression methods using Stata** have been produced by Stata Press. See [http://www.stata-press.com/books/](http://www.stata-press.com/books/)  
   **This includes:**
    -  Christopher Baum, [An Introduction to Modern Econometrics Using Stata](http://www.stata-press.com/books/modern-econometrics-stata/)

*   The web blog Marginal Revolution suggests some Stata Resources at
    [http://www.marginalrevolution.com/marginalrevolution/2011/01/stata-resources.html](http://www.marginalrevolution.com/marginalrevolution/2011/01/stata-resources.html)

*   Some talks at Stata User's group meetings are posted. See     [http://stata.com/meeting/](http://stata.com/meeting/)   
    This includes talks by me:
    - Estimating user-defined nonlinear regression models in Stata and in Mata: [cameronwcsug2008.pdf](http://cameron.econ.ucdavis.edu/stata/cameronwcsug2008.pdf)
    - Panel data methods for microeconmetrics using Stata: Updated version is     [trpanel.pdf](http://cameron.econ.ucdavis.edu/stata/trpanel.pdf)


*A. Colin Cameron / UC-Davis Economics /  [http://www.cameron.econ.ucdavis.edu/](http://cameron.econ.ucdavis.edu/)*


