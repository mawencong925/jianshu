![Stata连享会-研究生工具](http://upload-images.jianshu.io/upload_images/7692714-5005ee3a06a032cd.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

> 提要：配合使用 `EndNote`, `Word`, `Google学术`，提高你的文献管理效率；学点 `LaTeX`，提高你的审美能力；常用 `Markdown` 和`有道云笔记`，喜欢上写作和时刻记录你的想法。

- **文献管理**
  
  - **`EndNote`** 或 **`RefWorks`** 至少要会一个；  
  - 配合`百度学术`或`谷歌学术`，最好注册一个账号，以便自动记录检索历史；
  - 要学会配合使用 `EndNote+百度学术+Word`。
  - 有关 `EndNote` 的使用方法，参见 [Stata连享会-论文写作](http://www.jianshu.com/c/f1d2656c3812)

- **知识管理和文献梳理**

  可以使用`思维导图`。在 [知乎](https://www.zhihu.com/question/22094277) 中搜索关键词【思维导图】会有很多相关的讨论。

- **统计软件**

  - `Stata`，`R`，`Matlab` 至少会用一个。
  
  - 如果准备做量化或者基金之类的，也可以抽空学学 `Python`。

  - 具体学哪个，还要看你的最终用这些**武器**干啥。
  
- **写作和排版** 

  - `Word` 必须要学会，且能熟练排版出符合要求的文稿，可以看看侯捷先生的经典著作**Word的排版艺术**。
  
  - 有时间，可以学学 `LaTeX`，即使不用，也可以大幅提高你的审美能力。很多不是不想做好排版，而是不到什么是【美】。

- **日常记录**
  - **简书**。可以注册一个`简书`账号 (在线写，实时保存，随时随地可以查看修改)。看到好文可以一键分类收藏，形成自己的专辑。
  - **有道云笔记**。或为知笔记，或印象你笔记，功能都差不错，主要是实现多设备云平台写作，自动同步，随时写入，摘录网页和文献等。允许多人协作编辑文档。

- **Markdown**   
  
  - `Markdown` 一种简单的标记语言，让你在写作的同时不用纠结排版问题。花两个小时就能学会，上述在线笔记都支持 Markdown。  

  - `Markdown` 的最大好处是**一种输入，多种输出**。可以将 Markdown 文档直接贴入**简书**编辑器，后者会自动转化成美观的网页。
  
  - 也可以将 Markdown 文档转化为 Word 或 PDF 文档。`Stata` 官方和用户提供了多个命令用于支持 Markdown 文档，如 `dydoc`, `markdown`, `markstat`, `markdoc` 等。
  
  - 对 Markdown 感兴趣的同学，可以看看 [连玉君Markdown笔记](http://www.jianshu.com/p/4c27c66757a8)，以及[stata连享会-Markdown](http://www.jianshu.com/c/172844579b6c)

- **我的做法**
  
  - 我平时主要使用**有道云笔记**记录一些想法、文献摘要、网页摘录、新学到的东西等等。
  - 可以随时在电脑上，手机上，IPAD 上记录，支持语音录入。
  - 可以直接讲分享链接发给同学或老师，他可以直接存入自己的有道笔记中。也支持多人协作编辑。
  - 若是使用 `Markdown` 格式书写，贴入**`简书`**或**`知乎`**（使用 `Markdown Here` 插件渲染）即可分享给大家。

  
- **几个提高效率的小软件和技巧**

  - `Everything`。可以快速查询电脑中的文件，速度非常快。
  - `Q-dir`。多窗口文件管理。
  - 设置`搜狗`拼音输入法的【自定义短语配置】功能，大幅提高输入效率。[详细介绍](http://www.jianshu.com/p/4c27c66757a8)
  - 上述小软件下载地址：[`老连常用小软件`](https://gitee.com/arlionn/mysoftware)

> 参见：[Postgraduates & Research Support: Research Tools and Softwares](http://uj.ac.za.libguides.com/c.php?g=581189&p=4011584)

---
![Stata连享会二维码](http://wx1.sinaimg.cn/mw690/8abf9554gy1fj9p14l9lkj20m30d50u3.jpg "扫码关注 Stata 连享会")