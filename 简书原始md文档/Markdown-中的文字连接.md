Markdown 中插入链接的语法是：`[]()`。我此前一直是采用最原始的方式，逐个输入上述四个字母。现在做了些改进，效率明显提高了：

- **Case I：** 对于需要手动输入连接的部分，我会在[搜狗短语配置]([具体使用方法](https://gitee.com/arlionn/sougou))中设定一个短语，格式为：
```
lianj,1=[]()
```
随后若想输入 `[]()`，只需在搜狗输入法中文输入模式下敲入 `lianj`，然后敲空格即可录入 `[]()`，随后在分别填入链接文字和链接地址。   
&emsp;   

- **Case II：** 对于从网上复制过来的链接，则可以使用简书的 Markdown 编辑器写作，然后贴到 码云 或 有道云笔记 编辑器中。在简书的编辑器中，从百度、Yahoo 或其他网站粘贴过来的链接文字会自动转化为 `[链接文字](链接网址)` 这种 Markdown 格式。   
例如，复制下图中的 【协整关系-百度百科】到简书编辑器中，
![简书 Markdown 编辑器的神奇功能：粘贴链接文字，自动生成链接语句](https://gitee.com/uploads/images/2017/1030/091932_f612d4c1_1522177.png "屏幕截图.png")
会自动生成如下 Markdown 语句：
```
[*协整*关系_百度百科](http://www.baidu.com/link?url=OeMtzts1cMo7QbbOdqv83YpgRiaOtWyQ79OchuYWExREJ5ZJol5uWVWQNvCuUUvmpISGggmkezfdbbUvs72lb2SxgjEiWeIHCKyxGYyGFDmblU1YCMn-nlW1ZB7I7dVw)
```
输出效果为：[*协整*关系_百度百科](http://www.baidu.com/link?url=OeMtzts1cMo7QbbOdqv83YpgRiaOtWyQ79OchuYWExREJ5ZJol5uWVWQNvCuUUvmpISGggmkezfdbbUvs72lb2SxgjEiWeIHCKyxGYyGFDmblU1YCMn-nlW1ZB7I7dVw)