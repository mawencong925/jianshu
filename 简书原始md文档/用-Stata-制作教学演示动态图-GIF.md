![Amazing！ 这是用 Stata 制作的！](http://upload-images.jianshu.io/upload_images/7692714-f0fddf3966693de5.gif?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

> 你一定看过很多 GIF 动态图了，今天，来感受一下教学中的 GIF 动态图的视觉冲击效果！
>
>> **学生**：原来这么简单！ 我老师……我被老师……
>>
> >**老师**：还可以这样教？！
>
> ![正态分布的参数变化](http://upload-images.jianshu.io/upload_images/7692714-8c76b5ebac361648.gif?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
> ![The basic idea of lowess smoothing](http://upload-images.jianshu.io/upload_images/7692714-900aef815c745b8d.gif?imageMogr2/auto-orient/strip "lowess.gif")

> **制作思路：**利用人眼的记忆延迟特征形成视觉上的动态效果。
>1. 用循环语句一次性绘制多张参数渐变的图形，如 20 张；
>2. 利用 GIF 在线制作网站（如 [www.gif5.net](http://www.gif5.net/)）完成 GIF 图片制作：「上传图片」&rarr;「调整图片顺序」&rarr;「设定播放速度参数」&rarr;「开始制作」&rarr;「保存图片」。

#### 1. Stata 范例1：文首 GIF 图片的制作方法

>##### 1.1 核心语句

上面那张清新的图片，核心语句只有一个简单的循环：
```stata
forvalues i = 10(-1)0 {
    disp " `i'"
}
```
运行后，屏幕呈现效果如下：
```stata
. forvalues i = 10(-1)0 {
  2.     disp " `i'"
  3. }
 10
 9
 8
 7
 6
 5
 4
 3
 2
 1
 0
```

> ##### 1.2 核心思路
为了能在图片中显示上述数字，我们可以使用 Stata 的绘图命令 `twoway function`，假装绘制一幅函数图 `y = x`，然后将图形的颜色设定为白色，同时把图片的背景、外框等都设定为白色或隐藏模式。这样以来，最终输出的图片在视觉上就好像在白纸上写了几个字而已。通过下面的例子，大家可以学到很多有关 Stata 图形外观设定的选项。

> ##### 1.3 具体步骤
- **第一步**，运行如下 Stata 命令，生成一组变参数图片，这些图片就是制作 GIF 的原料：
```stata
*-----------------------------------------Begin----
local j=301
forvalues i = 10(-1)0 {
  #d ;
    twoway function y = x, 
	 lcolor(white) 
     text(0.85 0.5 "Stata连享会", 
  	      size(*3.0) color(red*1.5) placement(n))
     text(0.7 0.53 "推文来了……", 
  	      size(*2.2) color(red*1.2) placement(n))		  
  	 text(0.3 0.5 "{stSans:`i'}", 
  	      size(*7.0) color(blue)    placement(n))
  	 xlabel(none) ylabel(none) 
	 ysize(5) xsize(4)
  	 xtitle("") ytitle("") 
  	 yscale(off) xscale(off) 
     graphregion(color(white) style(none)) 
	 plotregion(style(none))
  	 scheme(s1mono); 
    graph export LianXH_`j++'.png,  
          as(png) width(380) height(720) replace;	 
  #d cr
}
*-----------------------------------------Over----
*-Notes: 相关帮助文档
* help added_text_options //图片中的文字设定
* help graph text //字体，希腊字母等
* 设定图形默认字体，依次点击：  
*   [主界面 Edit --> Pref --> Graph Pref -> Font] 
```
最后一张图片有所不同，需要单独生成，命令如下：
```stata
* 生成最后一张图片
  #d ;
    local i=8;
    twoway function y = x, 
       text(0.7 0.53 "I Love", 
  	      size(*4.0) color(red*1.2)  placement(n))		  
  	   text(0.5 0.5 "Stata", 
  	      size(*5.0) color(blue) placement(n))
     lcolor(white) 
  	 xlabel(none) ylabel(none) 
	  ysize(5) xsize(4)
  	 xtitle("") ytitle("") 
  	 yscale(off) xscale(off) 
     graphregion(color(white) style(none)) 
	 plotregion(style(none))
  	 scheme(s1mono); 
    graph export LianXH_312.png,  
          as(png) width(380) height(720) replace;		 
   #d cr 
```
完成上述两步后，文件夹中自动输出了如下图片（共 12 张）：
>![](http://upload-images.jianshu.io/upload_images/7692714-48b3826970803895.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

**第二步**，在线制作 GIF 图片。
  - **S1:** 打开 GIF 在线制作网站（如 [www.gif5.net](http://www.gif5.net/)），选择「**添加图片**」，导入上述图片（可以自行设定图片的**宽**度和**高**度，以及**延迟**时间）；
 - **S2:** 点击「**开始生成gif**」；
 - **S3:** 等待片刻，**下载（保存）**生成的图片即可：
> ![GIF在线制作方法 www.gif5.net](http://upload-images.jianshu.io/upload_images/7692714-ef600a80809ecbc4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



#### 2. Stata 范例 二：正态分布之教学演示
具体做法同范例 1，不再赘述，仅列出制作单张图片所需的 Stata 命令。
```stata
*-----------------------------------------Begin-----------
local GraphCounter = 301
local mu_label = 0.45
local power_label = 2.10
local mu_null = 0
local mu_alt = 2

forvalues sd = 1(-0.01)0.5 {
  local z_crit = round(-1*invnormal(0.05)*`sd', 0.01)
  local z_crit_label = `z_crit' + 0.75
  
                 #delimit ; 
  twoway                                                          
    function y=normalden(x,`mu_null',`sd'),                       
             range(-3 `z_crit') color(red) dropline(0)    ||        
    function y=normalden(x,`mu_alt',`sd'),                        
             range(-3 5) color(green)  dropline(`mu_alt') || 
    function y=normalden(x,`mu_alt',`sd'),                        
             range(`z_crit' 6) recast(area) color(green)  || 
    function y=normalden(x,`mu_null',`sd'),                       
             range(`z_crit' 6) recast(area) color(red)            
    title("Power for {&mu}={&mu}{subscript:0} versus {&mu}={&mu}{subscript:A}")
    xtitle("{it: z}") xlabel(-3 -2 -1 0 1 2 3 4 5 6)              
    legend(off)                                                   
    ytitle("Density") yscale(range(0 0.6))                        
    ylabel(0(0.1)0.6, angle(horizontal) nogrid)                   
    text(`mu_label' 0 "{&mu}{subscript:0}", color(red))           
    text(`mu_label' `mu_alt' "{&mu}{subscript:A}", color(green)) ;  
   graph export mu_alt_`GraphCounter'.png,  ///
         as(png) width(1280) height(720) replace
                           ;
                 #delimit cr

  local ++GraphCounter
  local mu_label = `mu_label' + 0.005
  local power_label = `power_label' + 0.03
}
*-----------------------------------------Over------------
```
>![The power increases as the sample size increases](http://upload-images.jianshu.io/upload_images/7692714-ddee738506b9d0d1.gif?imageMogr2/auto-orient/strip "ChangeSampleSize.gif")



#### 3. Stata 范例三：滚动窗口回归系数的图形可视化
具体做法同范例 1，不再赘述，仅列出制作单张图片所需的 Stata 命令。
```stata
*-----------------------------------------Begin-----------
sysuse auto, clear
local WindowWidth = 500
forvalues WindowUpper = 2200(25)5000 {
  local WindowLower = `WindowUpper' - `WindowWidth'
  twoway (scatter mpg weight)                                          ///
    (lowess mpg weight if weight < (`WindowUpper'-250), lcolor(green)) ///
    (lfit mpg weight if weight>`WindowLower' & weight<`WindowUpper',   ///
         lwidth(medium) lcolor(red))                                   ///
    , xline(`WindowLower' `WindowUpper', lwidth(medium) lcolor(black)) ///
    legend(on order(1 2 3) cols(3))
  graph export lowess_`WindowUpper'.png, as(png)  ///
        width(1280) height(720) replace
}
*-----------------------------------------Over------------
```
> ![The basic idea of lowess smoothing](http://upload-images.jianshu.io/upload_images/7692714-900aef815c745b8d.gif?imageMogr2/auto-orient/strip "lowess.gif")

**本文受如下文章启发，特此致谢！**
Stata Blogs >> [How to create animated graphics using Stata](https://blog.stata.com/2014/03/24/how-to-create-animated-graphics-using-stata/)     
（作者：[Chuck Huber, Senior Statistician](https://blog.stata.com/author/chuber/ "Posts by Chuck Huber, Senior Statistician")）

>#### 关于我们
- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [【简书-Stata连享会】](http://www.jianshu.com/u/69a30474ef33) 和 [【知乎-连玉君Stata专栏】](https://www.zhihu.com/people/arlionn)。可以在**简书**和**知乎**中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 推文中的相关数据和程序，以及 [Markdown 格式原文](https://gitee.com/arlionn/jianshu) 可以在 [【Stata连享会-码云】](https://gitee.com/arlionn) 中获取。[【Stata连享会-码云】](https://gitee.com/arlionn) 中还放置了诸多 Stata 资源和程序。如 [Stata命令导航](https://gitee.com/arlionn/stata/wikis/Home) ||  [stata-fundamentals](https://gitee.com/arlionn/stata-fundamentals) ||  [Propensity-score-matching-in-stata](https://gitee.com/arlionn/propensity-score-matching-in-stata) || [Stata-Training](https://gitee.com/arlionn/StataTraining) 等。


>#### 联系我们
- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com


---
>![欢迎加入Stata连享会(公众号: StataChina)](http://upload-images.jianshu.io/upload_images/7692714-177605b43f0456fb.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")

---
---
---
> Stata 寒假班  **报名中……**
连玉君主讲，`2018年1月13日-21日`，北京）   
[Stata**初级班**](http://www.peixun.net/view/307.html)&ensp;｜&ensp;[Stata**高级班**](http://www.peixun.net/view/308.html)&ensp; | &ensp;[Stata**全程班**](http://www.peixun.net/view/1135.html) 
---
---
---
